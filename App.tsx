import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';

import { Button, ProgressBarAndroidBase, ProgressBarAndroidComponent, StyleSheet, Text, TextInput, View  } from 'react-native';


export default function App() {

  const [isHungry, setIsHungry] = useState(true);

  return (
    
    <View style={styles.container}>
      <Text style={styles.appbar}><AppBar name="MyApp"></AppBar></Text>
      <Text style={styles.name}>Login {isHungry} </Text>
      <TextInput style={styles.inputbox}  placeholder='username' editable={true}/>
      <TextInput style={styles.inputbox}  placeholder='password' editable={true}/>
      <Button  title='Login'  onPress={() => {
    setIsHungry(false);
  }} ></Button>
  <ProgressBarAndroidComponent></ProgressBarAndroidComponent>
      <StatusBar style="auto" />
    </View>
  );
}

const AppBar = (props:any) => {
  return <Text>{props.name}</ Text>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  name:
  {
    color:'#f00',
    fontSize:28
  },
  inputbox :
  {
    width:250,
    height:50,
    margin:10,
    borderColor:'#000',
    borderWidth:1,
    padding:5
  },
  appbar:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 30,
    height:60,
    padding:10,
    backgroundColor:'#0ff',width:'auto',fontSize:18
  },
  button1:{
      width:100,
      height:50
  }
});
